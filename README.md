# My dots

I regularly use and create new virtual machines, and I use this repo to keep a base line of my dot files availible for quick setups of my basic enviroment. In VMs in which I install X Windows I use i3 along with my config files in '~/.config/i3'.

##Emacs

Emacs is my editor of choice, and I use my associated .emacs.d repo at https://github.com/mjfeller/.emacs.d. I'm looking into the possibilities of creating a new set of configuration files for my server installation.
