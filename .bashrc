# .bashrc

# Path
export PATH=/usr/local/bin:/usr/bin:/bin:/usr/sbin:

# Java and Junit
export JAVA_HOME=/Library/Java/Home
export ANDROID_HOME=/usr/local/sdk/
export JUNIT_HOME=/Library/JUNIT
export CLASSPATH=$CLASSPATH:$JUNIT_HOME/junit4.10.jar:.

# Editor
export EDITOR=/usr/bin/emacs

# Plan9
export PLAN9=/usr/local/plan9 export PLAN9
export PATH=$PATH:$PLAN9/bin:$HOME/.local/bin:$HOME/bin

# Source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
alias ll='ls -laF'
alias ..='cd ..'
alias c='clear'
